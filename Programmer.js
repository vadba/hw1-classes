class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(salary) {
        super._salary = salary * 3;
    }

    get salary() {
        return this._salary;
    }
}
